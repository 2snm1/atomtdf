package com.tdf.two;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class WordReverse {
	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) {
			System.out.println("Do you want to try the app? ");
			System.out.println("Press enter to start");
			System.out.println("Enter 'exit' to exit");
			while (!input.nextLine().equalsIgnoreCase("exit")) {
				System.out.println("Please enter a sentence to be reversed");
				reverseSentence(input.nextLine());
			}

		}
	}

	/**
	 * function that reverses a sentences and prints it out.
	 * 
	 * @param input
	 *            the user input
	 */
	private static void reverseSentence(String input) {
		List<String> inputList = Arrays.asList(input.split(" "));
		List<String> outputList = new ArrayList<String>();
		for (int index = inputList.size() - 1; index >= 0; index--) {
			outputList.add(inputList.get(index));
		}
		String result = "";
		for (String out : outputList) {
			result += out + " ";
		}
		
		System.out.printf("The reverse of your input: %s \nis  %s", input, result);
		System.out.println("\npress enter to do it again or type 'exit' to exit");
	}
}
