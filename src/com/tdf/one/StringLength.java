package com.tdf.one;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StringLength {

	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) {

			// System.out.println("Enter 'Y' for yes or 'N' for No");
			System.out.println("Do you want to try the app? ");
			System.out.println("Press enter to start");
			System.out.println("Enter 'exit' to exit");
			while (!input.nextLine().equalsIgnoreCase("exit")) {
				System.out.println("Please enter an input string");
				String data = input.nextLine();
				int length = stringLength(data);
				if (length > 0) {
					System.out.printf("the last word has %d letters", length);
				} else {
					System.out
							.println("There is no last word (You either entered a sentence with one word or an empty string)");
				}

				System.out.println("\nTo exit please enter the word exit or press enter to do it again");
			}

		}

	}

	/**
	 * method that returns the length of the last word in a sentence separated
	 * by ' ',if the sentence has one word then it will return 0;
	 * 
	 * @param input the string entered by the user
	 * @return the length of the last word in the sentence
	 */
	private static int stringLength(String input) {
		List<String> wordList = Arrays.asList(input.split(" "));
		if (wordList != null && !wordList.isEmpty() && wordList.size() > 1) {
			int lastIndex = wordList.size() - 1;
			return wordList.get(lastIndex).length();
		}

		return 0;
	}

}
