package com.tdf.three;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PreceedZeros {
	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) {
			System.out.println("Do you want to try the app? ");
			System.out.println("Press enter to start");
			System.out.println("Enter 'exit' to exit");

			while (!input.nextLine().equalsIgnoreCase("exit")) {
				System.out
						.println("Please enter an input string eg. 1,23,6778 \nie. a number separated by commas ',' \nthe number will pe padded by zeros eg. the above response is 000001,000023,006778");
				String data = input.nextLine();
				if (data == null || data.isEmpty()) {
					System.out.println("Please provide an input");
					continue;
				}
				outputNumber(data);
				System.out.println("To exit please enter the word exit or press enter to do it again");
			}
		}
	}

	/**
	 * this function prepends '0's to a number separated by commas eg if the
	 * user inputs 1,23,6778 the function will output 000001,000023,006778
	 * 
	 * @param data
	 *            the user input
	 */
	private static void outputNumber(String data) {

		List<String> wordList = Arrays.asList(data.split(","));
		List<String> appendedWordList = new ArrayList<>();
		for (String value : wordList) {
			if (value.trim().length() < 6) {
				int zeroAppends = 6 - value.length();
				String appendZero = "";
				for (int index = 0; index < zeroAppends; index++) {
					appendZero += "0";
				}
				appendedWordList.add(appendZero + value);
			} else {
				appendedWordList.add(value);
			}

		}
		String response = "";
		for (String out : appendedWordList) {
			response += out + ",";
		}
		//incase user enters 1,
		if (response.lastIndexOf(",") == response.length()-1) {
			response += "000000";
		}
		System.out.println("The padded output is: "+response);
	}
}
